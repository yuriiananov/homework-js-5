1. Опишіть своїми словами, що таке метод об’єкту.

Метод об'єкту це функція, яка знаходиться в середині об'єкта, через виклик якої можна здійснювати певні дії з об'єктом або його властивостями.

2. Який тип даних може мати значення властивості об’єкта?

Властивості об'єкта можуть мати будь який тип даних, від примітивних типів і до складних об'єктів (функції, масиви, об'єкти).

3. Об’єкт це посилальний тип даних. Що означає це поняття?

Коли ми створюємо об'єкт та визначаємо його змінній, ця змінна не має значення об'єкта, а зберігає посилання на цей об'єкт у пам'яті.
