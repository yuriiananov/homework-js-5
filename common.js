"use strict";

/*Завдання
Реалізувати функцію створення об’єкта “юзер”. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

  Технічні вимоги:
1. Написати функцію createNewUser(), яка буде створювати та повертати об’єкт newUser.
2. При виклику функція повинна запитати ім’я та прізвище.
3. Використовуючи дані, введені юзером, створити об’єкт newUser з властивостями firstName та lastName.
4. Додати в об’єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з’єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
5. Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
  Необов’язкове завдання підвищеної складності
6. Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.*/

function createNewUser() {
  let firstName = prompt("Введіть своє ім'я:");
  let lastName = prompt("Введіть своє прізвище:");

const newUser = {
  firstName,
  lastName,

  getLogin: () => `${firstName[0].toLowerCase()}${lastName.toLowerCase()}`
  };
return newUser;
}

const user = createNewUser();
console.log(user.getLogin());